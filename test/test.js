var request  = require('supertest');

var app = require('../index.js');

describe('GET /', function(){
    it('displays "aPure Web App is up and running ... "', function(done){
        request(app).get('/').expect('aPure Web App is up and running ... ', done);
    });
});