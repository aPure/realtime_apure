module.exports = {
    webserver: {
        staticfiles_path: process.env.STATIC_FILES || '/',
        web_url: process.env.WEB_URL || 'localhost'
    },
    database: {
        dbpassword: process.env.DB_PWD || '0000'
    }
}