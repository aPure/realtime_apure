var dev_mode = false;
var ctx = document.getElementById("myChart");
users_numbers = users_numbers.substr(1);
day_dates = day_dates.substr(1);
users_numbers = users_numbers.split(',').map(function(item){
    return parseInt(item);
});
day_dates = day_dates.split(',');
per_hours_only_ss = per_hours_only_ss.split('-').map(function(item){
    return parseInt(item);
});
per_hours_without_ss = per_hours_without_ss.split('-').map(function(item){
    return parseInt(item);
});
//console.log(per_hours_only_ss);
//console.log(per_hours_without_ss);
$(function () {
    var socket = io({
        path: dev_mode ? '/socket.io' : '/users/socket.io',
        transports: ['polling'],
        upgrades: ['polling']
    });
    //console.log(socket);
    socket.on('today_users', function(result){
        var current = $('.users_t').text();
        $('.users_t').text(result);
    });
});

var myLineChart = new Chart(ctx, {
    type: 'line',
            data: {
                labels: day_dates.reverse(),
                datasets: [{
                    label: "Users Numbers trends",
                    backgroundColor: 'rgba(0, 255, 0, 1)',
                    borderColor: 'rgba(0, 255, 0, 1)',
                    data: users_numbers.reverse(),
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Each day'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Number of users'
                        }
                    }]
                }
            }
});

var barChartData = {
    labels: ["0h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "11h", "12h", "13h", "14h", "15h", "16h", "17h", "18h", "19h", "20h", "21h", "22h", "23h"],
    datasets: [{
        label: 'Monday - Friday',
        backgroundColor: 'rgba(98,134,62, 1)',
        data: per_hours_without_ss
    }, {
        label: 'Saturday - Sunday',
        backgroundColor: 'rgba(165,209,129, 1)',
        data: per_hours_only_ss
    }]
};
var cty = document.getElementById("myChart_3").getContext("2d");
var stackedBar = new Chart(cty, {
    type: 'bar',
    data: barChartData,
    options: {
        title:{
            display:true,
            text:"Each hour"
        },
        tooltips: {
            mode: 'index',
            intersect: false
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        }
    }
});