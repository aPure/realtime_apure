const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const bodyParser = require('body-parser');
const config = require('./config');
const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : config.database.dbpassword,
  database : 'apure_events'
});

connection.connect(function(err){
    if(err) throw err;
    console.log("Connected to db!");
});


app.set('views', __dirname + '/views')
app.set('view engine', 'jade')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/', express.static('public'));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    next();
});

app.get('/admin', function(req, res){
  res.render('admin/index', {
        title: 'Home'
  });
});

app.get('/admin/referral', function(req, res){
    res.render('admin/referral', {
          title: 'Referral'
    });
});

app.get('/admin/data', function(req, res){
    res.render('admin/data', {
          title: 'Data'
    });
});

app.get('/admin/geterp', function(req, res){
  //console.log(req.query["month"]);
  const erp = require('./ERP_Scraping/erp.js');
  erp(req.query["month"], parseInt(req.query["start_date"]), parseInt(req.query["end_date"]), parseInt(req.query["num_tabs"]), function(){
    console.log("Fini");
  });
  res.render('admin/data', {
        title: 'Data'
  });
});

app.get('/', function(req, res){
  res.send('aPure Web App is up and running ... ');
});


app.post('/referral', function(req, res){
  console.log(req.body.email + ", refered by : " + req.body.referral);
  res.render('thanks_note', {
        title: 'Referral'
  });
});

app.get('/referral/:email/', function(req, res){
  console.log(req.params.email);
  res.render('referral', {
        email: req.params.email,
        title: 'Referral'
  });
});

app.get('/prepare_referral', function(req, res){
  res.render('prepare_referral', {
        title: 'Prepare Referral'
  });
});

app.post('/prepare_referral/', function(req, res){
  console.log(req.body.phone);
  var link = "http://localhost:3000/referral/"+req.body.email;
  res.render('copy_message', {
        link: link,
        title: 'Copy Message'
  });
});

app.post('/referral', function(req, res){
  console.log(req.params);
  res.render('referral', {
        title: 'Referral'
  });
});

app.get('/this_day', function(req, res){
  var query = "call getAllUsersDay";
  connection.query(query, function(err, result) {
    if (err) throw err;
    var users_numbers = result[0][0].users_number;
    var day_dates = result[0][0].day_dates;
    var per_hours_only_sat_sun = result[0][0].per_hours_only_sat_sun;
    var per_hours_without_sat_sun = result[0][0].per_hours_without_sat_sun;
    //console.log(per_hours_only_sat_sun, per_hours_without_sat_sun);
    res.render('users_this_day',{
      web_url: config.webserver.web_url,
      static_files_path: config.webserver.staticfiles_path,
      yesterday_users: result[0][0].yesterday_users,
      today_users: result[0][0].today_users,
      users_numbers: users_numbers,
      day_dates: day_dates,
      per_hours_only_sat_sun: per_hours_only_sat_sun,
      per_hours_without_sat_sun: per_hours_without_sat_sun
    });
  });
});

app.get('/update_users_this_day', function(req, res){
  var query = "select count(*) as today_users from users where time >= concat(curdate(), ' 09:00:00') and time <=now()";
  connection.query(query, function(err, result) {
    if (err) throw err;
    //console.log(result);
    io.emit('today_users', result[0].today_users);
  });
  res.send("OK");
});

io.on('connection', function(socket){
  // var query = "select count(*) as today_users from users where time >= concat(curdate(), ' 09:00:00') and time <=now()";
  // connection.query(query, function(err, result) {
  //   if (err) throw err;
  //   //console.log(result);
  //   //io.emit('today_users', result);
  // });
   
  //  socket.on('chat message', function(msg){
  //   io.emit('chat message', "the message ... "+msg);
  //  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

module.exports = app;